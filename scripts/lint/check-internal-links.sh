#!/bin/bash
#
# Controlla i link interni al sito.
#

files_match() {
  local page="$1"
  local np=$(/bin/ls -1d src/${page}* 2>/dev/null | wc -l)
  test $np -gt 0
  return $?
}

page_exists() {
  local page="$1"
  case "$page" in
    # Whitelist some known URLs.
    /static/*) ;;
    /pannello|/pannello/*) ;;
    /u/services*) ;;
    *)
        files_match "$page"
	return $?
        ;;
  esac
  return 0
}

./scripts/find-links.sh \
    | grep ^/ \
    | while read page; do
	page_exists "$page" || echo "$page is missing" >&2
      done

