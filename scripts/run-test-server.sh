#!/bin/bash
#
# Run a local test instance of the aiwebsite server.
#
# Usage: ./run-test-server.sh
#

http_port=3300

set -e

die() {
    echo >&2
    echo "ERROR: $*" >&2
    exit 1
}

find_installed() {
    which "$1"
}

is_installed() {
    test -n "$(find_installed "$1")"
}

stop() {
    local daemons="sitesearch test-httpd"
    local pidfile
    for d in $daemons ; do
        for pidfile in "${root_dir}/${d}.pid" "${root_dir}/build/apache/test/${d}.pid"; do
            if [ -e "${pidfile}" ]; then
                echo "stopping ${d}..." >&2
                kill -TERM $(cat "${pidfile}") || true
                rm -f "${pidfile}"
                break
            fi
        done
    done
}

run_in_chroot() {
    local chroot="$1"
    local user="$2"
    shift 2
    local cmd="$*"
    # Escalate privileges to run chroot, drop them to run the command.
    sudo chroot "${chroot}" su "${user}" --shell /bin/sh -c "${cmd}"
}

# Find absolute path of the top-level website directory.
script_dir=$(dirname "$0")
script_dir=${script_dir:-.}
root_dir=$(cd "${script_dir}/.." && pwd)
cd "${root_dir}"

# Fix up PATH so that is picks up sitesearch if it has been installed
# in build/.
export PATH="$PATH:${root_dir}/build/bin:/usr/sbin:/sbin/:/bin"

# Find Apache. Check the local chroot first, in which case copy the
# website contents within it and set TESTROOT accordingly...
apache2=
apache_dir="${root_dir}"
apache_user=`id -un`
chroot=
if [ -e "${root_dir}/build/apache/usr/sbin/apache2" ]; then
  apache2="/usr/sbin/apache2"
  apache_dir="/test"
  chroot="run_in_chroot ${root_dir}/build/apache ${apache_user}"
  cp "${root_dir}/test-httpd.conf" "${root_dir}/build/apache/test/test-httpd.conf"
else
  for prog in apache2 httpd; do
    if is_installed ${prog}; then
      apache2=${prog}
      break
    fi
  done
fi

test -n "${apache2}" || die "Could not find apache2. Try running 'sudo ./scripts/install-apache.sh'."

echo "Prerequisites checked."

stop

if [ "$1" = "stop" ]; then
    exit 0
fi

echo "Rendering website..."
"${script_dir}/update.sh"

# Copy files into the chroot _after_ generating the new pages.
if [ -e "${root_dir}/build/apache/usr/sbin/apache2" ]; then
  rsync -ar --delete "${root_dir}/public/" "${root_dir}/build/apache/test/public/"
fi

# Start daemons.
echo "Starting daemons in the background..."
sh -c "echo \$\$ > sitesearch.pid ; exec sitesearch --templates='${root_dir}/templates' --http=127.0.0.1:3301" &
${chroot} /usr/bin/env "PORT=${http_port}" "TESTROOT=${apache_dir}" \
    ${apache2} -f "${apache_dir}/test-httpd.conf"

echo
echo "Website is ready at http://localhost:${http_port}/"
echo
exit 0
