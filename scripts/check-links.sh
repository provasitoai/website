#!/bin/sh
#
# Local link checker. Runs by default against a local test instance,
# but it can be pointed at any live website using a command-line argument.
#

base_url=${1:-http://localhost:3300}

script_dir="$(dirname $0)"
script_dir="${script_dir:-.}"

"${script_dir}/find-links.sh" \
  | grep ^/ \
  | (while read url ; do
	curl -f -s -o- -L "${base_url}${url}" >/dev/null \
	|| echo "${url}    404"
     done)

