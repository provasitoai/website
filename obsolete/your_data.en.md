title: Your Data
----

Your Data
=============


v1.0 - Oct 2018

This document applies to all services managed by the AI-ODV, a non-profit organization. 

services includes: emails, websites, blogs, mailing lists, newsletters, irc, jabber, anonymous remailer and nym server.



### How we operate as an organization:

A/I is a non-commercial project ran by volunteers.

All services are provided "as is", in a "best effort" fashion, without receiving any form of payment in exchange. Volunteers working for the project do not receive any form of compensation for their effort or their expenses.

The cost of the infrastructure is covered with money received from spontaneous donations in a nontransactional relationship with the free services we provide. 

The principles behind the project are those of solidarity and self-organization.

Service requests from potential new users are satisfied depending on what we judge is their affinity with our manifesto and policy. 

We don't advertise our project and don't encourage anyone to use our services.

We are not trying to expand our infrastructure or our user base.


### What do we do with your data:

We require/provide a username (mail address) and a password to identify and authorize the account holder in order to access the services offered by A/I. 
All additional information on any of the services provided by A/I is supplied and controlled directly by the data subject out of free will. 
We don't require and in fact discourage the users from storing any identifier or personal data that could link to their physical identity on our servers. 

Our processing of your information is limited to storing it for you to use.

We store logs of your activity for a period up to 6 months (unless specified otherwise per service). 
This data is used to help diagnose software issues, maintain security of the system against intrusion, and monitor the health of the platform. 
However, these logs do not include IP addresses or identifiers or other personal data, in fact they are actively processed in order to make sure they are anonymized.

Further access to your personal data and stored files and other information you provide to any of the services offered by A/I is under your control.

We use disk encryption on all data to prevent data leak in cases where servers are stolen, confiscated, or in any way physically tampered with.
 
We provide and require SSL/TLS encryption on all provided services.



### What we do not do with your data:

We do not collect any data other than what is needed to provide you the service (that is your user: your email address).
We discourage you to use your real name (or other personal identifier, such a user on another internet provider connected to your real identity) as your username / email address, and we have no way to determine if that is the case or not. We hope and assume is not.

We do not in any way process, analyze your behavior or personal characteristics (profiling). We have no advertisements or business relationships with advertisers.

We do not share your data to third party unless in case of network inter-operatable (federated) services require certain data to operate (eg. other email service provider needs to know your email address to be able to deliver emails).
In those cases the choice and act of sharing the data is actively operated by you, and we have no way to stop you from doing that. In order to preserve your anonymity, we discourage you from doing that.

We do not sell any data to any third party. 

We do not send any marketing related information to our users (we don't have reason to do so: we don't sell our services).

We do not require any additional information that is not crucial for operation of the service (we do not ask for additional email addresses, phone numbers, street address or any other identifier that could correlate
your email address to your real identity). 

We do not access your data, emails, files etc. stored on our servers unless needed for troubleshooting purposes, or under suspicion of breaking our policy.

In the case of troubleshooting, we ask for your permission previously to the act of accessing your data and inform you afterwards of all actions taken against the account in the transparency report addressed to account holder.

In the case of suspicion of behaviour non-compliant to our policy, we might kindly ask the user to comply or decide to permanently erase an account without notice: again, users are admitted conditionally to their compliance to our policy and what we judge is their affinity to our manifesto.   

We do not store or transfer any personal data outside of the EU, and we have direct and exclusive access to all the dedicated servers where the data is stored in encrypted format. Communication between all servers is encrypted with "state of the art" protocols. We don't use any public cloud providers as AWS, Google cloud, Digital Ocean or the like. 


### Access to your information:

Federation.
Some of the services provided by A/I such as Email and Xmpp chat are operating based on so called Federation Protocols. This enables users signed up at different service providers to interact with each other. Because of the nature of the protocols (ability to send each other messages, share files, chat) some of the data is naturally shared with other entities. However, sharing data with other service provider is the user's choice and is configured by the users in their settings per service including the decision of with whom and what to share.

You may be shown embedded videos and link previews from other websites while using services provided by A/I. This may expose you to web tracking by external services, such as (but not limited to) Facebook, Twitter, and Google.
Again, in order to fully preserve your anonymity, you should hold your A/I account and other accounts separated. If that is not what you intend to do, your A/I email address could become an identifier of your physical person on other systems that are not under our control and responsibility. 

All data and files stored on services that are bound to personal information (services that require logging in) are available for you to download for either archival purposes or to transfer to another compatible website.



### Your Rights

Under the General Data Protection Regulation (GDPR) and The Data Protection Act 2018 (DPA) you have a number of rights with regard to your personal data. You have the right to request from us access to and rectification or erasure of your personal data, the right to restrict processing, object to processing as well as in certain circumstances the right to data portability. If you have provided consent for the processing of your data you have the right (in certain circumstances) to withdraw that consent at any time which will not affect the lawfulness of the processing before your consent was withdrawn. However, as explained previously, none of our services require and we in fact discourage you from providing any personal data to us. Therefore, we have never asked for your consent to process your personal data, that is in fact willingly stored by you in your personal user space and not accessed or processed by us in any other way from the pure storage. You can at any time dispose of all the data you have provided to us directly, including downloading and erasing it permanently. 

You have the right to lodge a complaint to the Information Commissioners’ Office if you believe that we have not complied with the requirements of the GDPR or DPA 18 with regard to your personal data. Identity and contact details of controller and data protection officer:

AI-ODV is the controller of data for the purposes of the DPA 18 and GDPR. 3 If you have any concerns as to how your data is processed you can contact:

- info@autistici.org - General contact email
- associazione@ai-odv.org - Official contact email

You can disable your mail account from your user panel. This does not completely delete the mail address from our system to avoid that someone else could ask for the same address in future.
Contact us if you prefer a complete deletion.
When the mail account is disabled, the mailbox content will be automatically erased within 3 days. Websites, mailing lists and blogs managed by that mail account will remain active, unless you deactivate them personally or require the deactivation to us subsequently to your act of deactivation of your corresponding email account.

If you need to remove some personal information from a mailing list public archive or some website/blog hosted on our platform contact us.


Freely adapted from the disroot.org privacy policy
