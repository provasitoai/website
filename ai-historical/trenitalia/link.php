<html>
<head>
  <title>Altri link su Trenitalia</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">



<h1>Licenziamenti, disservizi, reclami. Le storie di Trenitalia</h1><br>

</p>
<h4>Sui licenziamenti di trenitalia</h4>
<p>
<br/>
<a href="http://www.repubblica.it/2004/a/sezioni/cronaca/assunti/assunti/assunti.html">http://www.repubblica.it/2004/a/sezioni/cronaca/assunti/assunti/assunti.html</a>
<br/>
<a href="http://www.report.rai.it/argomenti.asp?cat=3">http://www.report.rai.it/argomenti.asp?cat=3</a>
<br/>
<a href="http://www.cub.it/htm-FLTU/trenitalia-ritirare%20i%20licenziamenti.htm">http://www.cub.it/htm-FLTU/trenitalia-ritirare%20i%20licenziamenti.htm</a>
<br/>
<a href="http://www.hideout.it/index.php3?page=notizia&id=541">http://www.hideout.it/index.php3?page=notizia&id=541</a>
<br/>
<a href="http://it.geocities.com/cen_doc_lotta/archilotte/ferrovieri170104.htm">http://it.geocities.com/cen_doc_lotta/archilotte/ferrovieri170104.htm</a>
<a href="http://www.fiom.cgil.it/ferroviario/ansaldo/c_090104.htm">http://www.fiom.cgil.it/ferroviario/ansaldo/c_090104.htm</a>
</p>

<h4>Trenitalia e  guerra</h4>
<br>
<a href="http://www.albasasso.it/documenti/item.asp?id=68">http://www.albasasso.it/documenti/item.asp?id=68</a>
<br/>
<a href="http://www.sfairos.it/trenitalia.htm">http://www.sfairos.it/trenitalia.htm</a>
<br/>
<a href="http://www.altroadige.it/News/view_content?news_id=20030410124400">http://www.altroadige.it/News/view_content?news_id=20030410124400</a>
<br/>
<p>
Se vuoi aiutare a mettere in evidenza quest'ennesimo sopruso 
fai circolare questi link o linkane alcuni sul tuo sito.
</p>
<br>

<h4>Altri Link</h4>

<a href="http://punto-informatico.it/p.asp?i=49297">http://punto-informatico.it/p.asp?i=49297</a>
<br/>
<a href="http://www.autistici.org/zenmai23/trenitalia/">http://www.autistici.org/zenmai23/trenitalia/</a>
<br/>
<a href="http://dicorinto.it/archives/000267.html">http://dicorinto.it/archives/000267.html</a>
<br/>
<a href="http://italy.indymedia.org/news/2004/08/600418.php">http://italy.indymedia.org/news/2004/08/600418.php</a>
<br/>


<a href="http://www.kuht.it/modules/news/article.php?storyid=1139">http://www.kuht.it/modules/news/article.php?storyid=1139</a>
<br/>
<a href="http://www.ilsecolodellarete.it/html/modules.php?op=modload&name=News&file=article&sid=40">http://www.ilsecolodellarete.it/html/modules.php?op=modload&name=News&file=article&sid=40</a>
<br/>
<a href="http://www.montorionet.it/modules/news/article.php?storyid=96">http://www.montorionet.it/modules/news/article.php?storyid=96</a>
<br/>
<a href="http://www.fabiopani.it/?module=blog&timeid=040830183626">http://www.fabiopani.it/?module=blog&timeid=040830183626</a>
<br/>
<a href="http://wup.it/article.php?sid=6368">http://wup.it/article.php?sid=6368</a>
<br/>
<a href="http://italy.indymedia.org/features/cyber/">http://italy.indymedia.org/features/cyber/</a>
<br/>
<a href="http://navigando.blogspot.com/2004/08/trenitalia-e-censura.html">http://navigando.blogspot.com/2004/08/trenitalia-e-censura.html</a>
<br/>
<a href="http://www.forumodena.org/modules.php?op=modload&name=News&file=article&sid=610">http://www.forumodena.org/modules.php?op=modload&name=News&file=article&sid=610</a>
<br/>
<a href="http://www.socialpress.it/breve.php3?id_breve=375">http://www.socialpress.it/breve.php3?id_breve=375</a>
<br/>
<a href="http://ilgrandeboh.zapto.org/index.php?p=1036">http://ilgrandeboh.zapto.org/index.php?p=1036</a>
<br/>
<a href="http://galiza.indymedia.org/ler.php?numero=9481&cidade=1">http://galiza.indymedia.org/ler.php?numero=9481&cidade=1</a>
<br/>
<a href="http://estrecho.indymedia.org/news/2004/08/8704.php">http://estrecho.indymedia.org/news/2004/08/8704.php</a>
<br/>
<a href="http://www.cmaq.net/es/node.php?id=17824">http://www.cmaq.net/es/node.php?id=17824</a>
<br/>

<h4>Sulle lamentele</h4>

<a href="http://www.ainfos.ca/03/aug/ainfos00309.html">http://www.ainfos.ca/03/aug/ainfos00309.html</a>
<br/>
<a href="http://www.pieroruzzante.it/rassegna/articoli2004/febbraio/08-1.htm">http://www.pieroruzzante.it/rassegna/articoli2004/febbraio/08-1.htm</a>
<br/>
<a href="http://digilander.libero.it/pendolaripiacenza/documenti/Archivio/lettere_2001_2002.htm">http://digilander.libero.it/pendolaripiacenza/documenti/Archivio/lettere_2001_2002.htm</a>
<br/>
<a href="http://www.triburibelli.net/sito/modules/MyAnnonces/index.php?pa=viewannonces&lid=6691">http://www.triburibelli.net/sito/modules/MyAnnonces/index.php?pa=viewannonces&lid=6691</a>
<br/>



  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
