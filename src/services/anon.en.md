title: Anonymity services
----

Anonymity services
==================

This section introduces you to the services offered by our servers directly aimed at safekeeping the privacy and anonymity of our users.

If you are reading this page, you have probably already thought about how precious your anonymity in the Internet is, otherwise you can have
a look at this article: [The eternal value of privacy](http://www.wired.com/politics/security/commentary/securitymatters/2006/05/70886)

So we invite you to get more details on our anonymity services and to check out our [blog](https://cavallette.noblogs.org), which is often
ripe in news on privacy defense, new cutting edge softwares and on the most blatant attack on our privacy.

You can also check some of the manuals we have written on this subject in our [Anonymity Howto Section](/docs/anon/).

Anonymous Remailer
------------------

An anonymous remailer is essentially a service to hide one's identity when sending messages by e-mail. A message sent using an anonymous
remailer is not traceble back to you, and for this reason it can't receive a reply.

You can get more info on the [remailer web page](https://remailer.paranoici.org) and on our [Remailer Howto](/docs/anon/remailer).

Nym Server
----------

A nymserver provides long-term pseudonymous identities, it's based on anonymous remailer network but it allows to create a virtual address
that can send and receive messages anonymously.

You can get more info on the [remailer web page](https://remailer.paranoici.org/nym.php).
