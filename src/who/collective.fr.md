title: Une petite histoire de A/I et de ses malheurs
----

Une petite histoire de A/I et de ses malheurs
=============================================

Le collectif A/I (qui signifie [autistici/inventati](/who/manifesto)) est né en mars 2001 lorsque des individus et des
collectifs en rapport avec la technologie, la vie privée, les droits numériques et l'activisme politique se sont rassemblés en Italie. Notre
but principal est de fournir des outils de communication gratuit à une large base, tout en poussant les gens à choisir des modes de
communication libres plutôt que commerciaux. Nous tenons à éveiller la conscience des gens sur la nécessité de protéger leur vie privée et à
échapper au pillage qui est indistinctement perpétré par des gouvernements et des sociétés sur les données numérique et personnelles.

Plus de dix ans après la mise en place de notre premier serveur, A/I héberge environ 10.000 boites mails, 3000 blogs, 2500 mailing lists et
1500 sites web. En dehors de ces services, nous proposons un service gratuit de messagerie instantanée cryptée pour jabber, des espaces de
discussion IRC, un anonymiseur de mail et un réseau privé virtuel (VPN). Nous avons voyagé à travers l'Italie et l'Europe pour expliquer et
transmettre nos idées sur les moyens de communication numérique. A ce jour, la communauté surfant sur les eaux sombre des services de A/I
atteint le millier de personnes.

Ce n'est pas grand chose, surtout si l'on considère le sombre scénario mondial contre lequel nous nous battons, mais c'est suffisant pour
attirer une attention non désirée, la censure et une repression ouverte : dès 2003 nous avons reçu une demande de remise des données
personnelles de certains utilisateurs de messagerie personnelle, mais comme tout le monde le sait, nous suivons une politique stricte de non
rétention de logs ou de quelconques informations concernant l'identité des personnes qui utilisent nos services. Ainsi, les requerants ne
peuvent rien obtenir de notre part, et c'est ainsi depuis notre création. En 2004 l'un des site web que nous hébergions (Zenmai)\[[1](#1)\]
a été fermé lorsque Trenitalia (la compagnie de chemins de fer italienne) trouva que la blague que notre utilisateur avait faite sur leur
appui au transport de véhicules de l'armée n'était pas vraiment drôle, mais après quelques déboires judiciaires, nous avons réussi à obtenir
gain de cause. \[Maintenant ce site est à nouveau en ligne et Google a une nouvelle histoire à raconter sur Trenitalia.\]

Cette même année, finalement, la police a saisi et copié le contenu de tous les disques de nos serveurs avec l'aide du prestataire qui les
hébergeait (Aruba) et sans que nous en sachions quoi que ce soit (nous l'avons découvert par hasard un an plus tard\[[2](#2)\] en examinant
les actes d'une affaire où on nous demandait de fermer une seule boite mail\[[3](#3)\]). Nous avons donc été forcés de trouver une stratégie
nous permettant d'effectuer une avancée significative dans la défense de la vie privée et des droits des personnes. C'est pourquoi nous
avons développé le [Plan R\*](/who/rplan/) et passé beaucoup de temps à [voyager à travers l'Italie](http://www.autistici.org/ai/kaostour/)
et l'Europe pour expliquer ce qu'il s'était passé et ce qui allait se passer ensuite.

Après 2005 nous avons été harcelés sans arrêt par les procureurs et les forces de sécurité (et même par le Vatican!\[[4](#4)\]) qui nous
demandaient de leur fournir des informations d'utilisateurs et des identités et nous sommes fiers de dire que nous avons toujours répondu:
désolés, mais nous ne les avons pas. Récemment (2010) des policiers très intelligents ont réussis à convaincre un juge d'ordonner la saisi
de trois serveurs dans trois pays différents pour voir si nous n'avions RÉELLEMENT aucune donnés sur les activités d'un utilisateur sur nos
serveurs\[[5](#5)\]. Après avoir dépensé beaucoup d'argent public (pour quelques graffitis sur un mur), le juge se retrouva avec un paquet
d'informations cryptées ne recelant aucunes informations utilisables, peut-être y réfléchira-t-il à deux fois avant de redonner une enquête
à ces policiers forts compétents.

A l'heure actuelle, nous continuons de surfer sur le sombre océan d'Internet avec les même idées qu'au commencement : tout le monde devrait
pouvoir utiliser gratuitement et sans crainte les outils de communication offerts par le monde numérique et les utiliser pour lutter contre
les injustices, le droit des peuples et la dignité. Et quelqu'un doit pouvoir permettre au gens de lutter avec les outils appropriées. Les
connaissances et le pouvoir doivent être partagés. Le contrôle et la repression doivent être combattus. Nous n'étions rien, nous serons
tout.

- - -
  
**Note**

* <a name="1"></a>\[1\] Voir le cas Trenitalia vs. zenmai23 et A/I: <http://www.autistici.org/ai/trenitalia/index.en.php>
* <a name="2"></a>\[2\] Voir le cas A/I Crackdown : [http://www.autistici.org/ai/crackdown](http://www.autistici.org/ai/crackdown/#en)
* <a name="3"></a>\[3\] Voir le cas de Crocenera Anarchica: <http://www.autistici.org/ai/crocenera/index.en.php>
* <a name="4"></a>\[4\] Voir le cas Pedopriest: <http://cavallette.noblogs.org/2007/07/611>
* <a name="5"></a>\[5\] Voir le cas Norwegian Crackdown: [Site Web spéciale](http://www.autistici.org/ai/crackdown-2010/#english-crackdown) - <http://cavallette.noblogs.org/2010/11/7029>


