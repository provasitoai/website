title: Infrastructure Costs
----

Infrastructure costs
====================

Nous pensons qu'il est juste pour les personnes qui soutiennent notre travail par leurs donations annuelles de savoir combien coûtent les
installations de autistici.org/inventati.org.

Ces coûts peuvent être divisés en deux grandes parties : les **coûts de connexions** pour chacune des antennes de l'installation d'a/i et
les **coûts de maintenance** (coût du matériels, changement de serveurs, et exceptionnellement des frais de déplacements pour ceux qui
répareront les désastres impromptues :)

**Connexion (par an)**

environ 8300 euro

**Frais bancaires (par an)**

environ 600 euro

**Administration (par an)** \[ enregistrement de nom de domaine, comptabilité, etc \]

environ 1540 euro

**Maintenance (par an)** \[ hardware, serveur, frais de déplacements, dépenses légales, urgences \]

environ 3300 euro

**TOTAL environ 13740 euro par an**

Ainsi chaque année nous dépensons plus ou moins **14,000** euros pour faire survivre l'infrastructure.

Tout le travail de maintenance fait par le collectif est à titre bénévole. **Nous éxistons grace aux dons des personnes utilisant nos services**.

[Faites un don](/donate) dés maintenant!


