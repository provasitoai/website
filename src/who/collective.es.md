title: Breve historia del Colectivo A/I
----

Breve historia del Colectivo A/I
================================

A/I nació hace más de 10 años cuando individuos (personas) y colectivos ocupad\*s en cuestiones de tecnología,
privacidad, derechos digitales y activismo político se reunieron en Italia. Nuestra idea básica es proveer una amplia variedad de
herramientas de comunicación libres, impulsando a las personas a elegir aquellas opciones libres en lugar de las comerciales. Queremos
informar y formar sobre la necesidad de proteger nuestra propia privacidad con el fin de evitar el saqueo que gobiernos y corporaciones
llevan a cabo de forma indiscriminada sobre nuestros datos y personalidades.

Hoy en día, más de diez años después de que nuestro primer servidor fuera activado, A/I aloja casi 10.000 casillas de correo, más de 1.500
sitios web, 3.000 blogs y aproximadamente 2.500 listas de distribución. Aparte de estos servicios, ofrecemos mensajería instantánea
encriptada y gratuita vía Jabber, salas de chat de IRC, un remailer anónimo de correos y VPN. Hemos viajado por Italia y Europa tratando de
explicar y de propagar nuestras ideas sobre comunicación digital. En este momento, la comunidad que surféa el oscuro mar de los servicios de
A/I asciende a miles de personas.

No son tantos, en particular si se considera el lúgubre escenario global contra el que estamos luchando, pero es suficiente para atraer
atención no deseada, censura y una clara represión: en 2003 recibimos una petición oficial de entregar información personal y del correo de
ciertos usuari\*s, pero como tod\*s saben, seguimos una política determinada de no
conservar ningún registro o información sobre la identidad de las personas utilizando nuestros servicios. Por lo que los inquisidores no
pudieron recibir esa información de nuestra parte, y así ha sido desde nuestro nacimiento. En 2004 uno de los sitios web que alojamos
(Zenmai)\[[1](#1)\] fue cerrado ya que Trenitalia (la empresa ferroviaria italiana) pensó que el chiste que nuestr\* usuari\* estaba
haciendo sobre el apoyo de esta empresa al transporte de vehículos militares
no era realmente gracioso, pero después de ciertos percances legales, logramos ganar el juicio. \[Ahora ese sitio web está otra vez online y
Google tiene una nueva historia que contar sobre Trenitalia.\]

Ese mismo año, finalmente, la policía confiscó y copió el contenido de todos los discos en nuestro servidor con la ayuda del proveedor que
lo alojaba (Aruba), sin nosotros saber nada sobre este hecho (lo descubrimos casualmente un año después\[[2](#2)\] mientras revisábamos las
actas del caso donde nos habían demandado cerrar una sola casilla de correo\[[3](#3)\]). Estuvimos entonces forzados a encontrar una
estrategia que nos permitiera tomar un paso importante en nuestra defensa de la privacidad y los derechos de las personas. Es por eso que ha
visto la luz el [Plan R\*](/who/rplan/ "The R Plan") y hemos pasado una gran cantidad de tiempo [viajando](http://www.autistici.org/ai/kaostour/)
por Italia y Europa para explicar lo que había sucedido y lo que pasaría luego.

Después de 2005 fuimos constantemente molestados por abogados y fuerzas de seguridad (¡inclusive por el Vaticano!\[[4](#4)\]) pidiéndonos
que entregáramos la información sobre la identidad de nuestr\*s usuari\*s y estamos
orgullosos de siempre haber podido contestar: “lo sentimos, pero no contamos con esa información.” Recientemente, (2010) un agente de
policía muy listo logró convencer a un juez para que ordenara la confiscación de tres servidores en tres países diferentes para averiguar si
*REALMENTE* no teníamos ninguna información en nuestros servidores sobre la actividad de ciert\* usuari\*\[[5](#5)\].
Después de gastar mucho dinero público (por un par de graffitis en una pared), el juez terminó con un
montón de archivos encriptados sin ninguna información útil dentro, y quizás lo pensará dos veces antes de asignarle otras investigaciones
al astuto policía.

Ahora mismo, nos vamos a surfear, en el oscuro océano de internet, con las mismas ideas con las que comenzamos: todos deberían poder usar
libremente y sin miedo las herramientas de comunicación ofrecidas por el mundo digital, explotarlas para luchar contra la injusticia y por
los derechos y la dignidad de las personas. Y alguien tiene que ser capaz de proveer a la gente que lucha con las herramientas apropiadas.
El conocimiento y el poder deberían ser compartidos. El control y la represión deberían ser combatidos. Los nada de hoy, todo han de ser.
(We have been nought, we shall be all.)

- - -
  
**Notas**
  
* <a name="1"></a>\[1\] Ver el caso Trenitalia vs. zenmai23 y A/I: <http://www.autistici.org/ai/trenitalia/index.en.php>  
* <a name="2"></a>\[2\] Ver el caso A/I Crackdown: <http://www.autistici.org/ai/crackdown/#en>  
* <a name="3"></a>\[3\] Ver el caso sobre Crocenera Anarchica: <http://www.autistici.org/ai/crocenera/index.en.php>  
* <a name="4"></a>\[4\] Ver el caso Pretofilia: <http://cavallette.noblogs.org/2007/07/611>  
* <a name="5"></a>\[5\] er el caso Norwegian Crackdown: [sitio web especial](http://www.autistici.org/ai/crackdown-2010) - <http://cavallette.noblogs.org/2010/11/7029>

10 años nerdcore: Un libro sobre A/I en las palabras de A/I.
============================================================

Hemos coleccionado algunas imágenes y palabras a lo largo de nuestra historia. Reserva tu libro ahora: info (at) autistici . org :)

