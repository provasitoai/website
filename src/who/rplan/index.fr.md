title: Le Plan R*
----

Le Plan R\*: un réseau de communication résistant
=================================================

![](/static/img/pianor_rete.png)

**Plan R\*** : un réseau de communication résistant

Le [collectif A/I](/about) est heureux de vous présenter le **Plan R\***.

Le **Plan R\*** est un outil pour développer un réseau de communication résistant.

Résistant car il est conçu pour prévenir au mieux (mais sans jamais se sentir invincible) les coupures de communication digitales
dispensées par nos services.

Mais aussi résistant parce qu’il est lié aux rêves d'une politique réaliste, aux conflits sociaux que nous subissons et au besoin d'outils
de communication pour diffuser et prospérer.

Dans cette section du site:

- nous souhaitons vous dire comment le Plan R\* à été conçu et développé,
- nous souhaitons vous expliquer ce qui nous pousse à développer le plan R\* aujourd’hui et quels événements nous assure que c'est urgent.
- nous tacherons de vous donner des instructions simples pour faire face à ce changement aisément et en apprécier les nouveautés. :)

Tout le monde peut contribuer au projet : si vous souhaitez nous suggérer des améliorations ou des détails particuliers à prendre en
considération , écrivez nous un mail à <info@autistici.org> (en utilisant la clef gpg disponible
[ici](/get_help#gpgkey "A/I GPG key") serait parfait). Mais la plus grande des contribution au droit à la vie privée est
de ne jamais compter sur quelqu'un pour votre propre sécurité : nous pouvons offrir la plus sure des transmission possible, mais la
meilleure des sécurité est celle dont vous vous en donnez les moyens. Et pour ça nous vous répéterons toujours que la manière la plus sure
de protéger vos communication est d'utiliser des moyens de chiffrage.

Si vous souhaitez en savoir plus à ce sujet, vous pouvez consulter ce [guide](http://help.riseup.net/mail/security/measures/) : avec de
bonnes habitudes de communication vous améliorerez considérablement la vie privé de tous, ce qui est une base essentielle pour **s'opposer
au contrôle de masse**, et vous transformerez aussi l’outil de communication en un instrument de révolte.

Commençons notre voyage:

- [Présentation du Plan R\*](/who/rplan/intro "introduction to the R* Plan")
- [Comment le Plan R\* fonctionne](/who/rplan/how "R* Plan mechanics")
- [Que signifie « Plan R\* »](/who/rplan/what "R* Plan reasons")
- [Diapositives résumant le Plan R\*](http://www.autistici.org/orangebook/slides/orangebook.en.html "R* Plan slides")

Le collectif A/I

Octobre 2005

