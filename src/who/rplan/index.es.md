title: The R* Plan
----

El Plan R\*: una red de comunicación resistente
===============================================

![](/static/img/pianor_rete.png)

**El Plan R\***: una red de comunicación resistente

El [colectivo A/I](/about) esta orgulloso de presentarte el **Plan R\***.

El **Plan R\*** es una herramienta para desarrollar una red de comunicacion resistente.

Resistente, ya que ha sido concebido para evitar tanto como sea posible (pero sin sentirse todopoderoso) fallas en la comunicación digital
ofrecidos por nuestros servicios.

Y resistente también porque se relaciona con el sueño de un conflicto político y social real que vive y necesita una herramienta de
comunicación para difundir y florecer.

Estamos abriendo este nuevo sitio debido a que:

- nos gustaría nos gustaria decirle cómo se concibió y desarrolló el Plan R\*,
- nos gustaría explicar lo que nos llevó a desarrollar el Plan como lo es ahora y los acontecimientos que nos convencieron de que era
    urgente,
- es fundamental para que la transición a la nueva estructura lo más fácil posible, y por lo tanto vamos a tratar de darle instrucciones
    sencillas para enfrentar el cambio tan fácilmente como sea posible, y para disfrutar de las novedades :)

Todos pueden contribuir al proyecto: Si tu deas sugerirnos alguna mejora o sugerencia, escribenos a <info@autistici.org> (usando la llave
GPG que puedes encontrar [aqui](/get_help#gpgkey "A/I GPG key") es la mejor forma de hacerlo!). Pero la mayor
contribucion para la privacidad de cualquiera consiste en nunca dejarle la responsabilidad a otros de sus seguridad: podemos ofrecer las
conexiones más seguras y privadas posibles, pero la mayor seguridad es la que se asegura por su cuenta. Es por eso que nunca dejaremos de
repetir que la forma más segura de proteger su privacidad de las comunicaciones es el cifrado.

Si desea saber más sobre este tema, puede comenzar a leer [este tutorial](http://help.riseup.net/mail/security/measures/): mediante la
difusión de hábitos de comunicación seguras, ayudará a la mayoría considerablemente salvaguardar la privacidad de todos, que es un requisito
fundamental para **oponerse control total**, y también se les dará la redes de comunicación en un instrumento de **lucha y disensión**.

Empecemos un Tour por el proyecto:

- [Presentación del Plan R\*](/who/rplan/intro "introduction to the R* Plan")
- [¿Como Funciona el Plan R\*?](/who/rplan/how "R* Plan mechanics")
- [¿Que significa el plan R\*?](/who/rplan/what "R* Plan reasons")
- [Diapositivas que resumen el Plan R\*](http://www.autistici.org/orangebook/slides/orangebook.en.html "R* Plan slides")

El colectivo A/I

Octubre 2005
