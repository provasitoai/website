title: Quem somos 
----

Quem somos nós
=========
A/I nasceu há mais de 10 anos para fornecer suporte na internet (através de hospedagem de websites, contas de e-mails, listas de e-mails, chat, mensageiros instantâneos, redirecionamento de e-mail anônimo, blogs e muitas outras coisas) para ativistas e coletivos das bases de movimentos sociais do mundo. Nossos princípios são bastante simples: o mundo não deve ser governado pelo dinheiro, mas deve ter suas bases em solidariedade, comunidade, ajuda mútua, direitos iguais e livres e justiça social. Fácil, não é? Nós apoiamos indivíduos, coletivos, comunidades, grupos e todos aqueles que as atividades políticas e sociais estejam alinhadas a essa visão de mundo e que partilhem conosco de alguns princípios fundamentais: **antifascismo, antirracismo, antissexismo, antimilitarismo**. Além disso, acrescentamos também um sentimento, uma atitude: o **profundo mal-estar diante da lógica do dinheiro**.

Os nossos recursos são possíveis graças às doações da comunidade que os utilizam. O trabalho que o coletivo faz para manter tudo funcionando é estritamente **voluntário**. Nós não hospedamos projetos comerciais que certamente vão se sentir mais confortáveis em outro lugar. Nós não hospedamos instituições e partidos políticos, que deveriam ser capazes de gerir seus próprios servidores com o dinheiro que eles têm, em vez de se alimentar de uma rede autogerenciada e voluntária como a nossa. Nossos serviços são pensados e configurados para **proteger a liberdade de expressão e a privacidade** de nossas/os usuárias/os: é por isso que nós não guardamos informações sensíveis sobre elas/es (isto é, logs, IPs, IDs, etc), nem fazemos nenhum tipo de rastreamento de comercial.

Saiba mais sobre nós e nossos objetivos:

* Nossa [política](/who/policy)
* Nossa [privacy policy](/who/privacy-policy)
* Um [breve conto](/who/telltale) sobre o porquê de sermos quem somos e por que fazemos o que fazemos.
* O [manifesto](/who/manifesto) que escrevemos em 2002
* Algumas palavras sobre nossa [história como um coletivo](/who/collective) (menos poesia e mais fatos)
* [O Livro sobre os 10 anos de historia do coletivo](/who/book)
* [Propaganda, flyers, banners e gráficos](/who/propaganda)
* O [Plano R*](/who/rplan)
* [Custos](/who/costs) de infra-estrutura

Nossos servidores são legalmente gerenciados pela [Associação Investici](mailto:associazione@ai-odv.org). Quaisquer outras questões devem ser endereçadas ao nosso e-mail contato: info em investici.org.
