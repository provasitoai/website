title: Autistici/Inventati GPG key 
----

Autistici/Inventati GPG key
==============


	pub   4096R/95753730 2017-09-23 [expires: 2022-09-22]
	      Key fingerprint = 3E11 5222 7755 D9BD AD2C  7094 B498 267F 9575 3730
	      uid                  Autistici / Inventati Staff <info@autistici.org>
	      sub   4096R/02420566 2017-09-23 [expires: 2022-09-22]

        -----BEGIN PGP PUBLIC KEY BLOCK-----

        mQINBFnGX7MBEAC7Upr59iMy1B8Id5vGMObz1QqG4NyNBdIt7FX1DS8DPEchMOkW
        gL5babvOo3wi8VNHfK4g0EKfAwbBTbazcCOSSUvMx09to4TMW7oJ34pUNj+jVAeW
        q76be4QiLr8Zx//8vRXZ01G67ziKnNWMSB8mSytDyiQ/u8vVozTulFZJH9SUl9r5
        F/PQvGATL9BndXxzZlRVAGLq2d64giOsooTTYfRtAPZVeGv7GfOBH3AZsBF5CBqD
        UOxcpdrO2/PcTCipApo9w2sCOJhbIIkgiNzkfto3hN1Z7Dj3ON21P+vlt7vYSiuw
        nmhKTUbfrv/kkziFVFMIoVPOqBGyqLBk1gU2HPGLm+i47Lf41Zo1pCsRFgtW4Jcv
        oD53mWxrF7b8TCSe7vpAy6vMm67tukFgW4FStCKszqbOyeFBa+IuCwTy2fKh0j0q
        IWO9OcSdBH6YaBae7mTVFzlkWUZgLZhgtEGKYsQqigK+FctemImclVm7VZoaDc35
        7T+Ybiq8PMc9YCwVU11G18h7XQK/Af4KygonGNxudD7ghFuYsEiAFSUpMKj31cbr
        zzH/KnxSHEmTo223Aw+NsG0u4PmsYXZGeQJk6eC3XQ/puB0vYoc1nckamTOWHpUl
        U3dbuSiU8+jkJDYlL6fowZa502LM2izrALTvOO+BCVbpFcO37oCtvS2VsQARAQAB
        tDBBdXRpc3RpY2kgLyBJbnZlbnRhdGkgU3RhZmYgPGluZm9AYXV0aXN0aWNpLm9y
        Zz6JAlQEEwEKAD4WIQQ+EVIid1XZva0scJS0mCZ/lXU3MAUCWcZfswIbAwUJCWYB
        gAULCQgHAwUVCgkICwUWAwIBAAIeAQIXgAAKCRC0mCZ/lXU3MJCKD/9Osgz/XLIX
        MslBHElLtks/ZvQQgtDVcGMOZ1maJeSgvfOOaJTD1dJYbLAhrs91Q1VbSDjZXWZh
        Oq4lM2rpMzHtWY7Mm/y/geQX3hPsr8ADlReD7qlk6WGosHvQ0RhKwcTffTkSCtgG
        skTmpY6qGOYy6m+vtRwki0KpqGtbrNfGrnzIUrM1WHfUfB0RMPoMDHsfM4guIf3C
        KLxK47TaISiw9f1Jr/FSXcJonRC6mgGMVX7qHwH6d/3RzE2YWJVRuS2vkYZSoHKf
        Z3HgoQv/E9afpQzB8lcIwanqw1mJfoo2OG0aOL/dHJgsFnuODoZrMI2ivE6FPrCI
        nA47gGewMdIxz5NNqOKcQ0r4709BH8wF+00GaBi5PKG8Iba8mnoo6WTkFJi2zJ8C
        T6dO0ZBtBS3f+U5mVGrrPeF4NzQ/35W+icfd4vOsOQCiaRIlXwcDnb/oYfB1o95S
        6p0JjUQHnnhK3lcaRCNDBgymwan10DvOx9+XB1bK3dq8TxQGkwM8w91npufbu7ZS
        oCAxSv1i6TIMhJBHL+KZ4pceaSvJOzdgV5d8aSd2bwbcXMFjq28vEbSeOTR/J6hc
        XWTXpbgXgjDpgrgPBBlDa+v1Qd3dz4LXuYMM9EEY0+5E+cCTMvPpsToyFuffCKDq
        l6IeRLsyeRnFQBfM/tuUoDzJPQL4g/B+HIkCMwQQAQoAHRYhBOMNVlAQnlNTIQS4
        edpzPVnZjanOBQJZxmDVAAoJENpzPVnZjanOB+kP/2/xTRAhzZ0r+ULyxXaCbo4S
        iJBDHabmhwMWTEepJwBI8avv1T9/o47U3Nit7BzvAMoR4tFdxwvONwE+3pWqkKTN
        XTdOGvB0FsiBztTgC7Mm3xbVOicPXJ6NgDCEHHGQN3WBEVChRM+cKbvUwKjsy1yi
        dYDZpK2WVKWoP15/NkdsfIS24jNBPFHtJgeN0+T6YdLzrxYIXDgOt6Zxs0GAb3rj
        rQumcrnZIob2NFHtdxV1Q0FRNx1Iwc9mP1VBy8qNC1ibAwkhI/ISaTsyviUZTk/2
        yGltfOWb0jMm1/u6sSVBZ9oucs0dUHefUGwyg0An2O8Bzie4pkt7dI+JtwZsLzcb
        DP/PDRjuz+9GDic0vm44iblY4xjQL1HEurwqaOuSghURYML8zdfWX4Llws1QpvHv
        yvSeYCPGP8Hf0cI6zTaaedopQ26y14MEds3j8AgLxnjsreNkUO8vsHYIVy88+4VJ
        VxW2bGel0RaQr83jGS3SLyn9YSVXe2BiQ4Lsl68ox6FSthdb7okwmvONTDEIpmlH
        guSeA0SZGDOg2iSAll/iWK812hXp3g7ACtsXBLxRXO34KSQf0GFQpP1L4tthbkuP
        4oeM5fmzbcpHS6DYkYYznxHrt/ujb7y9S6xcGyfcWcnEFJOFtKrO+5+nD6adxVp4
        1XLvak7kdhco/tTPFoMquQINBFnGX7MBEADzH41iz5pKnePOX67ZKfoQe1xodITH
        Z4/LMeJWizdmS5sghM9kb2dwqSf8hHBL7hoEjQGIIkvnMBqzt/AroiYoLgqWGUuT
        feh31AbROCQQa4Ni2egYLv/vRpw78mfT7LNQaCio1bVQS3sxI6vQ/jmvcoB4eAfk
        PIjRwn+zA+G7TTNuUm7cEWttVTLX5g2Af11KPQuRCKU5y3Vsbb9N3Ff1QmAW4o5w
        2HLduf+ZUre3JsHsU/S2ABSubZJyo6T0lMy6QYvP4KXLGyqENC4UxJUuwx5LLPIH
        DgMTCPu4+7C8VXoSCCFKpLiQUEOd3utjtzD2PR+0DXnTP/lkq2tqFkbUUIHvOLuR
        B/dax+jki9H2nLZaMJWQNcrb1kxKBX3bKV3fFnLpr5UzhPchUsd5YynZ967zvlc7
        0tlRzE7x35goJH7XC1vvULr+poKNBPB7Gc7tVbPL5xc/0NrLRf+WJOuFJVK+cYYg
        L4lVKvWvZ8c/J74TdOw3AvhTpqREx5fl3YUKcdpGZsfoncGRiYpi9SfSW5JTcwcT
        W9W/Ge/lSLwqwtpjR0d3q6SfWw2f++Tlcwi6fmpRvRxSkCZql3sI1SVXqnWwkrpt
        idno5JdzgQNsEn8lXaXqiCWCzRVL65qdkuCNvIMrpqQcqrYKbGVBFZ95GKXvrXz7
        D+Rq3O+Ix9ryfQARAQABiQI8BBgBCgAmFiEEPhFSIndV2b2tLHCUtJgmf5V1NzAF
        AlnGX7MCGwwFCQlmAYAACgkQtJgmf5V1NzC9Jg//U2B44VA7XocaqALAh1rMT/h5
        +80rdmqhz1nGhIbSWS3fqrl8oKADjsl4bfodD6J6U1kW5gBWY5+wvIwzwg/tATRV
        r2W9gvDHdGFwiAaOBKhzQMM6Xxr9vE/wl9UjmQu5Y7L7VvqaQxqI8JX/WdMGAXi2
        eK2WlqHwiomlUr/+yuf389pVJxkpB3d6y181emOJQbLgdpmfJ84tGsie+HG3yKZT
        TKRZlPLg+XxuaC+VATkyAJA585WkFwMw4qwnibQM/1a7W0S2kR9Pvoz48/ejtu0s
        5lg1YoDUVUiPTVsJUOyWq8YlC1jUjNv2BNqpI3Qli/Bv5uXzmR3Up12qd9//Q7PM
        yCDnJ4rOy5Xbw/igMyAn+w6ljc7KuuMxoX4WBZaU2+mjXXm0CApWe5FMggtAohhP
        YvsgbwbFJUDnETpuv5fiuXtEGaqTu6gFPKnuD534GBpPhQZNRiRoa8cx96T4SkIw
        qo9F9Yx2yFK6HDDlRmObFy3VLDwW4dZ0sBgh64wjNudO35WBjs2prKhdO1l05odH
        hKKdVttxwmCaaC9dbZb9xtpEHaT02kt/dNktbGt7WkS43+fpduAr4CqS+3TlfGWx
        6O4yt4GZbbC77Yc4wntK/KedcfTJOQuCArrciXwdy71BqSs/n/wzITE4+EsxSCQD
        M+BAwE88YfwO+MCbGZ4=
        =x7Us
        -----END PGP PUBLIC KEY BLOCK-----

