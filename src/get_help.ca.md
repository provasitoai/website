title: Don't panic!
----

Don't panic!
============

Autistici/Inventati help page
-----------------------------

En aquesta pàgina intentem explicar-te què pots fer quan penses que necessites la nostra ajuda. Hem creat algunes eines per intentar reduir
el tràfic de correus electrònics. Així que, abans d'escriure'ns, **et convidem calorosament al fet que trobis aquí la solució al teu
problema**.

- [Cavallette](https://cavallette.noblogs.org/) - News?
- [FAQ](/docs/faq/) - Preguntes freqüents (amb les seves corresponents respostes :) )
- [Manuals](/docs/) - Els manuals dels nostres serveis.
- [Helpdesk](http://helpdesk.autistici.org/) - Aquí pots enviar-nos una pregunta específica per al teu problema, i et contestarem l'abans que sigui possible (deixa un compte de correu al qual puguem contestar!)

Generalment, també pots trobar a algun\*s de nosaltres al canal de IRC **\#ai** del nostre [servidor
autistici.org](/docs/irc/). Amb temps, algú et donarà consells i informació, o podrà ajudar-te a resoldre
petits problemes tècnics.

<a name="gpgkey"></a>

Si estàs realment desesperat/da i no saps que fer, [escriu-nos](mailto:info@autistici.org), possiblement
usant nostra [clau GPG](gpg_key) (disponible a molts servidors de claus).
