title: Col·lectius radicals tecnològics 
----

Col·lectius radicals tecnològics
--------------------------------

### Servidors anònims i independents que ofereixen correu electrònic i altres serveis amb polítiques similars als nostres

- [ecn.org](http://www.ecn.org) (ECN és un lloc per relacionar-se i estar en contacte amb gent a la qual els canvis profunds en les nostres societats han dispersat, un lloc on trobar persones que no es rendeixen a la uniformitat ideològica i marginalitat actual, entre persones que busquen crear un moviment real, capaç de canviar les coses - itàlia)
- [free.de](http://www.free.de) (Un projecte alemany històric que ofereix comptes de correu, llistes de correu, i allotjament web per a activistes - alemanya)
- [indivia.net](http://indivia.net) (Llistes de correu, streaming d'àudio, allotjament web, IRC, correu electrònic, fòrums, DNS dinàmic a un servidor independent verd el nom del qual significa "amanida d'endívies" - itàlia)
- [oziosi.org](http://www.oziosi.org) (Oziosi.org reconeix la mandra, la lentitud i l'ociositat a la qual tranquil·lament poden aspirar homes i dones - itàlia)
- [resist.ca](http://resist.ca) (El col·lectiu Resist! és un grup d'activista de Vancouver que treballa per proveir serveis tècnics i comunicacions, informació i educació per a la comunitat activista. El col·lectiu Resist! (Resist!) i el projecte resist.ca van créixer a partir del vell col·lectiu TAO - canadà)
- [riseup.net](http://riseup.net) (riseup.net proveeix de correus electrònics, llestes i allotjament web per tot\*s aquell\*s treballant al canvi social alliberador. un projecte per crear alternatives democràtiques i pràctiques d'acte-determinació a través del control segur dels mitjans de comunicació - o.s. a.)
- [squat.net](http://squat.net) (squat!net és una revista internacional amb les cases okupadas com a tema principal, oferint serveis d'internet a espais alliberats - Països Baixos)
- [so36.net](http://so36.net) (servidor independent - alemanya)
- [nadir.org](http://nadir.org) (Un projecte darrere d'una revisió dels principis de l'Esquerra a través de la creació d'espais de comunicació i informació - alemanya)
- [aktivix.org](https://aktivix.org/) (independent server offering email, mailing lists and VPN)
- [boum.org](http://boum.org) (servidor anònim i independent - col·lectiu radicat en frança)
- [shelter.is](https://shelter.is) (tech collective providing solidarity driven privacy friendly services)
- [espiv.net](https://espiv.net) (Cybrigade is an autonomous collective with main point of report, social fights and how these are expressed in cyberspace, cybrigade is managing espiv.net's services. Providing e-mail accounts, mailing lists, blogs - greece)


### Una llista incompleta de projectes amb els quals compartim alguna cosa...
  
- [freaknet.org](http://www.freaknet.org) (un laboratori per experimentar amb tecnologies de la informació - catania, sicilia, itàlia)
- [interactivist.net](http://interactivist.net/) (un esforç col·laboratiu, un recurs de comunicació activista, un projecte de mitjans independents i un projecte per compartir tecnologies - o.s. a.)
- [mutualaid.org](http://mutualaid.org) (un servidor per a comunitats radicals - o.s. a.)
- [nodo50.org](http://nodo50.org) (un servidor autònom - espanya)
- [tao.ca](http://tao.ca) (tao.ca és un grup anarquista d'un petit col·lectiu d'àrea de Torontó conegut com OAT, que va evolucionar des del TAO a Torontó i està relacionat amb altres grups activistes com resist.ca i interactivist.net - canadà)

