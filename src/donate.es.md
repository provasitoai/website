title: Como contribuir con el proyecto 
----

Como contribuir con el proyecto
================================

Considerando que este proyecto vive **EXCLUSIVAMENTE** gracias a las contribuciones, estaremos muy felices con cada gesto y contribución que
puedan hacer.  En ésta página se pueden encontrar muchas maneras de donar: ¡no seas tímid\*!. Si quieres saber cuanto cuesta mantener
nuestro servicio funcionando, mira[aquí!](/who/costs)

- [Transferencia Directa a Banca Etica](#bonifico)
- [Donación on-line](#carta) (es necesaria una tarjeta de crédito)
- [Entrega en mano](#life) (a quien conozcas de confianza del colectivo)

Detalles
--------

### Transferencia Directa a Banca Etica

<a name="bonifico"></a>

    Associazione AI ODV 
    EU IBAN: IT83P0501802800000016719213 
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Agencia de Milán
    V. Santa Tecla, 5
    Detalle/Razón: Donación para Investici (año 201x)
    
### Donación on-line

<a name="carta"></a>

Puedes efectuar tu donación a través de una tarjeta de crédito. Para la transacción usamos PayPal (no necesitas una cuenta específica), no
nos resulta muy simpático, pero no hemos encontrado una mejor alternativa hasta ahora. Si conoces alguna, por favor háznosla saber.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input name="on0" value="Per" type="hidden">Para</td>
<td align="left"><select name="os0"> <option selected="selected" value="E-mail">E-mail</option> <option value="Mailing-List">Listas de correo</option> <option value="Hosting">Alojamiento</option> <option value="NoBlogs">NoBlogs</option> <option value="Revolutions!">Revolutions!</option> </select> <input size="6" name="amount" value="25.00" type="text"> <small>(en Euros)</small></td>
</tr>
</tbody>
</table>

  
<input name="cmd" value="_xclick" type="hidden"> <input name="business" value="donate@inventati.org" type="hidden">
<input name="item_name" value="Donazione per A/I Servers" type="hidden"> <input name="item_number" value="10011" type="hidden">
<!-- input type="hidden" name="amount" value="25.00" / --> <input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden"> <input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden"> <input name="lc" value="IT" type="hidden">
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but11.gif" border="0" name="submit" alt="[Donate]">
</form>

### Suscripciones periódicas

Esto es una donación periódica. Si estás segur\* de suscribirte y donar a nuestro proyecto anualmente, puedes
pulsar el botón de "suscribe" del siguiente formulario y todos los años 50 euros (o la cifra que decidas) será transferida a nuestras arcas
:).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input name="on0" value="Perche'" type="hidden">For</td>
<td align="left"><select name="os0"> <option selected="selected" value="E-mail">E-mail</option> <option value="Mailing-List">Listas de correo</option> <option value="Web-Hosting">Alojamiento</option> <option value="NoBlogs">NoBlogs</option> <option value="Legal Support">Apoyo legal</option> <option value="Revolution!">Revolution!</option> </select> <input size="6" name="a3" value="50.00" type="text"> <small>(en Euros)</small></td>
</tr>
</tbody>
</table>

<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]">
<input name="cmd" value="_xclick-subscriptions" type="hidden"> <input name="business" value="donate@inventati.org" type="hidden">
<input name="item_name" value="Sottoscrizione Ricorsiva ad
A/I" type="hidden"> <input name="item_number" value="10012" type="hidden"> <input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden"> <input name="currency_code" value="EUR" type="hidden">
<input name="lc" value="IT" type="hidden"> <!-- input type="hidden" name="a3" value="50.00" / --> <input name="p3" value="1" type="hidden">
<input name="t3" value="Y" type="hidden"> <input name="src" value="1" type="hidden"> <input name="sra" value="1" type="hidden">
</form>

### Entrega en mano

<a name="life"></a>

Si vives cerca de alguien que forma parte del proyecto y en quien confías, pueeds darle tu donación  
Es importante que intentes recolectar diferentes donaciones (de colectivos, amig\*s u equipos) antes de darle el
dinero, así se logra una reducción en el número de pagos y transacciones realizadas.
