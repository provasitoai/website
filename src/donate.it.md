title: Come contribuire al progetto
----

Come contribuire al progetto
============================

Considerato che il progetto vive **ESCLUSIVAMENTE** grazie alle sottoscrizioni, saremo molto felici per ogni piccolo gesto che farete.
Vi ricordiamo ( a scanso di equivoci ) che le vostre donazioni non corrispondono a un pagamento per i servizi che forniamo.
Non e' ne' necessario ne' sufficiente effettuare una donazione per ottenere un nostro servizio.
I nostri servizi sono gratuiti ed esclusivamente destinati a tutte le persone che condividono i principi di **antifascismo, antirazzismo, antisessismo, antiomofobia, antitransfobia, antimilitarismo** 
e che non intendono utilizzarli a scopi commerciali. Per questo ricordiamo che effettuare una donazione e' un gesto **volontario** e **incondizionato**.

Data questa premessa, in questa pagina troverete molti modi per donare: non siate timidi!
Se volete sapere quanto costa mantenere i nostri servizi, date un'occhiata [qui!](/who/costs)
Non accettiamo Bitcoin. In [questo post](http://cavallette.noblogs.org/2013/07/8333) spieghiamo perché.

- [Bonifico bancario su Banca Etica](#bonifico)
- [Donazione online](#carta) (necessaria carta di credito)
- [Consegna a mano](#life) a qualcuno di tua fiducia del collettivo

Dettagli per effettuare i versamenti
------------------------------------

### Bonifico bancario

<a name="bonifico"></a>

    ASSOCIAZIONE AI ODV
    EU IBAN: IT83P0501802800000016719213 
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Filiale di Milano
    V. Santa Tecla, 5
    Causale: donazione investici (anno 201x)
    
### Donazione online

<a name="carta"></a>

Puoi effettuare la donazione direttamente utilizzando una carta di credito. Per la transazione utilizziamo PayPal (non è necessario aver un
account specifico), che non ci sta molto simpatico, ma purtroppo non abbiamo trovato alternative.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Per">Per</td>
<td align="left"><select name="os0"><option value="E-mail">E-mail<option value="Mailing-List">Mailing-List<option value="Hosting">Hosting<option value="Revolutions!">Revolutions!</select> <input type="text" size="6" name="amount" value="25.00"> <small>(in euro)</small></td>
</tr>
</tbody>
</table>

<input type="hidden" name="cmd" value="_xclick"> <input type="hidden" name="business" value="donate@inventati.org">
<input type="hidden" name="item_name" value="Donazione per Autistici/Inventati Servers">
<input type="hidden" name="item_number" value="10011"> <!-- input type="hidden" name="amount" value="25.00" -->
<input type="hidden" name="no_shipping" value="1"> <input type="hidden" name="no_note" value="1">
<input type="hidden" name="currency_code" value="EUR"> <input type="hidden" name="tax" value="0"> <input type="hidden" name="lc" value="IT">
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but11.gif" border="0" name="submit" alt="[Donate]">
</form>

### Sottoscrizione periodica

Questo è una sottoscrizione ricorsiva. Se vuoi essere sicuro di contribuire al progetto ogni anno, puoi procedere con il bottone "subscribe"
e ogni anno 50 euro (o la cifra che deciderai) verranno trasferiti nei nostri forzieri :).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input type="hidden" name="on0" value="Perche'">Perché</td>
<td align="left"><select name="os0"><option value="E-mail">E-mail<option value="Mailing-List">Mailing-List<option value="Web-Hosting">Web-Hosting<option value="Legal Support">Legal Support<option value="Revolution!">Revolution!</select> <input type="text" size="6" name="a3" value="50.00"> <small>(in euro)</small></td>
</tr>
</tbody>
</table>

<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]">
<input type="hidden" name="cmd" value="_xclick-subscriptions"> <input type="hidden" name="business" value="donate@inventati.org">
<input type="hidden" name="item_name" value="Sottoscrizione Ricorsiva ad Autistici/Inventati">
<input type="hidden" name="item_number" value="10012"> <input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="no_note" value="1"> <input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="lc" value="IT"> <!-- input type="hidden" name="a3" value="50.00" --> <input type="hidden" name="p3" value="1">
<input type="hidden" name="t3" value="Y"> <input type="hidden" name="src" value="1"> <input type="hidden" name="sra" value="1">
</form>

### Consegna a mano

<a name="life"></a>

Se conosci qualcuno nella tua città, di tua fiducia, che è coinvolto nel progetto A/I, consegna di persona la tua sottoscrizione.  
 In questo caso sarebbe importante cercare di raccogliere più di una sottoscrizione alla volta (collettivi, gruppi di amici, crew), in modo
che alla consegna a mano consegua anche una riduzione del numero dei versamenti effettuati.

