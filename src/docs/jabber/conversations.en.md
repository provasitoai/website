title: Conversations Configuration Howto
----

Conversations Configuration Howto
=================================


Conversations is an instant chat client for Android 4.0+ smartphones. It is
based on the XMPP protocol and therefore it can be used with the chat services
provided by Autistici/Inventati. It enjoys several strengths on its side.

First and foremost, it respects a _sine qua non_ for us: this software is
released under the **GPL 3** license and thus [its
code](https://github.com/siacs/Conversations) is freely searchable and
modifiable.

For what concerns security issues, Conversations implements the use of three
different **protocols for end-to-end communications encryption**: OpenPGP
(experimental support), [OTR](https://otr.cypherpunks.ca/) and
[OMEMO](https://conversations.im/omemo/).

Still in this regard, the software created by _inputmice_ enjoys **a permissions
management which is not an invasive one**: connectivity, vibration (in order to
receive notices) and little else. If you decide to integrate your jabber contact
list with the phone book, the app will also have access your contacts: in any
case, these will stay on your smartphone and will be not uploaded on third
parties servers.

Another important feature to emphasize is that the developers’ team made the
application freely available on [F-Droid](https://f-droid.org/). This means that
Conversations can also be downloaded by everyone using a Play Store-free Android
version in order to safeguard their own privacy and that of their peers.

Finally, this messaging client has a **very low impact on battery energy
consumption** and a **clean and easily manageable user interface**.

Let’s see how to configure it in a few steps.


## Basic Configuration ##

Once it is installed, you can open Conversations: the first screen you will
see will be this one:

![](/static/img/man_jabber/conversations/Conversations_1.png)


Select _"Use my own provider"_. This second window will then open up.

![](/static/img/man_jabber/conversations/Conversations_2.png)


Insert:

*   Your username (i.e. username@autistici.org) under _"Jabber ID"_,

*   Your account password under _"Password"_ (or a specific password for Jabber
    if you activated the [Two Factor
    Authentication](/docs/2FA) on our
    services),

There you go! Press the _"Next"_ button and you will be ready
to use Conversations with the AI jabber server.


## Configuration through Hidden Service ##

If you want to achieve further privacy and security, you can access Jabber using
our [Tor hidden service](/docs/anon/tor).

In order to do that, download the Orbot application from
[F-Droid](https://f-droid.org/repository/browse/?fdfilter=orbot&fdid=org.torproject.android)
or from the [Play
Store](https://play.google.com/store/apps/details?id=org.torproject.android).
Once it has been launched, there are some simple changes to be made to
Conversations’ basic configuration.

Open Conversations’ main screen and select _"Use my own provider"_. This second
window will then open up.

![](/static/img/man_jabber/conversations/Conversations_2.png)

For the time being, do not insert your username or your password. Press the
three dots in the upper-right corner: you will thus access the
application’s options menu. Select _"Expert Settings"_.

![](/static/img/man_jabber/conversations/Conversations_3.png)

Scroll the window downwards to the _"Connection"_ section and select the options
_"Extended Connection Settings"_ and _"Connect via Tor"_.

![](/static/img/man_jabber/conversations/Conversations_4.png)

Go back to the first screen and insert

*   Your username (i.e. username@autistici.org) under _"Jabber ID"_,

*   Your account password under _"Password"_ (or a specific password for Jabber
    if you activated the [Two Factor
    Autenthication](/docs/2FA) on our
    services),

*   The address of Autistici/Inventati’s XMPP hidden service
    - autinv5q6en4gpf4.onion - under _"Hostname"_,

*   The server port (5222) under _"Port"_.

![](/static/img/man_jabber/conversations/Conversations_5.png)


Done! Now you can use A/I’s Hidden Service in order to chat with your friends on
Jabber.


## Using OTR with Conversations##

As we already mentioned, Conversations implements many protocols for end-to-end
encryption, focused on ensuring a greater privacy for your communications.
Let’s see how to use [OTR](https://otr.cypherpunks.ca) with Conversations.

The first time you open a dialog box with a user in your contact list,
press the lock in the menu above and select _"OTR"_.

![](/static/img/man_jabber/conversations/Conversations_6.png)

Write a first message and send it selecting the arrow icon bottom left.
Conversations will automatically start the encryption keys exchange between you
and your peer. From now on, the conversations with this contact will be
automatically encrypted.

![](/static/img/man_jabber/conversations/Conversations_7.png)


**Do not forget to verify the authenticity of the key!**


## Using OMEMO ##

Conversations implements another protocol called
[OMEMO](https://conversations.im/omemo/) for enabling end-to-end encrypted
communications. Unlike OTR, OMEMO provides some interesting functions, like
encrypted group chats, message synchronization among different devices and
off-line delivery. Let's see how to use it.

The first time you open a dialog box with a user in your contact list,
press the lock in the above menu and select _"OMEMO"_.

![](/static/img/man_jabber/conversations/Conversations_8.png)

Write a first message and send it selecting the arrow icon bottom left.
Conversations will automatically start the encryption keys exchange between you
and your peers. From now on, the conversations with this contact will be
automatically encrypted.

![](/static/img/man_jabber/conversations/Conversations_9.png)

**Do not forget to verify the authenticity of the key!**
