title: Adium Jabber Configuration Howto (Mac OSX only)
----

Adium Jabber Configuration Howto (Mac OSX only)
===================================================

Install [Adium](http://www.adium.im) and launch it.

Select the Preferences menu

![adium preferences panel](/static/img/man_jabber/it/adium-01-accounts.png)

Click on the plus sign (low left corner) and choose "Jabber".

Now you'll be prompted by the "Account" panel

![adium accounts panel](/static/img/man_jabber/it/adium-02-jabber_account.png)

In the "Jabber ID" field fill in you email address. It would be better not to fill in your password at this stage (because it would be saved
in your Preferences and thus in your filesystem): you can always type it every time you launch Adium.

Click on the "Personal" tab and fill in a nickname and an image to represent you.

![adium personal panel](/static/img/man_jabber/it/adium-03-preferences-personal.png)

Click on the "Options" tab and fill in the servername "jabber.autistici.org", the port "5222" and mark the checkbox "Use TLS Encryption".

![adium options panel](/static/img/man_jabber/it/adium-04-preferences-options.png)

We could configure many other things but these are more than enough to get started. Click on "OK", type your password and start chatting on
jabber :)
