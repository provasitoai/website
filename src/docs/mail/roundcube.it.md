title: Manuale per l'utilizzo della webmail (basata sul software libero roundcube)
----

Manuale per l'utilizzo della webmail (basata sul software libero roundcube)
==================================================================================

- [Come accedere alla webmail](#login)
- [Come inserire contatti nella rubrica](#rubrica1)
- [Come inviare mail a più destinatari](#rubrica2)
- [Non vedo la cartella coi messaggi inviati o con le bozze](#cartelle)

<a name="login"></a>

Come accedere alla webmail
--------------------------

Per accedere alla webmail potete fare il login direttamente dalla homepage [www.autistici.org](/) come indicato:

![](/static/img/man_mail/it/login.jpg)

A questo punto potete leggere la posta cliccando su "Leggi la posta"

![](/static/img/man_mail/it/leggi_posta.jpg)

<a name="rubrica1"></a>

Rubrica
-------

Per inserire un nuovo contatto nella rubrica cliccate su *Rubrica* in alto a destra e poi sull'icona col segno *+* (*Aggiungi un contatto*).

![](/static/img/man_mail/it/rubrica_vuota.jpg)

Compilate i campi che compaiono nella parte destra dello schermo (Nome e Cognome possono essere trascurati) e cliccate su *Salva*

![](/static/img/man_mail/it/primo_contatto.jpg)

![](/static/img/man_mail/it/come_compare.jpg)

![](/static/img/man_mail/it/inserire_contatto.jpg)

<a name="rubrica2"></a>

**Per inviare un messaggio a più destinatari** cliccare su *Rubrica* in alto a destra

![](/static/img/man_mail/it/destinatari.jpg)

![](/static/img/man_mail/it/seleziona_indirizzi.jpg)

![](/static/img/man_mail/it/invia_mail_con_piu_indirizzi_.jpg)

<a name="cartelle"></a>

Cartelle
--------

**Se non vedete le cartelle dei messaggi inviati o delle bozze:**

![](/static/img/man_mail/it/mancano_cartelle_1.jpg)

![](/static/img/man_mail/it/mancano_cartelle_2.jpg)

![](/static/img/man_mail/it/mancano_cartelle_3.jpg)

Se la cartella *Sent* o *Drafts* non esistono potete crearle inserendo il nome nella casella che c'è un fondo alla pagina e cliccando su
*Crea*.

![](/static/img/man_mail/it/mancano_cartelle_4.jpg)
