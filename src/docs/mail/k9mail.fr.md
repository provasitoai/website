title: Configuration de K-9 Mail
----

Configuration de K-9 Mail
========================================

Configurer votre compte.

![](/static/img/man_mail/fr/k92.jpg)

La demande d’identifiants sera affichée par défaut si aucun compte n’est configuré.
 Si un compte est déjà configure, utiliser la touché options et “Ajouter un compte”.
 Vous arriverez alors sur cette page et devrait remplir les champs, correspondants à votre adresse e-mail et votre mot de passe.

Choisissez maintenant le type de protocole utilisé pour la réception des mails.

![](/static/img/man_mail/fr/k93.jpg)

IMAP est intéressant si votre client mail K-9 n’est pas votre principal, puisqu’il consulte et agit directement sur le serveur.
 Au contraire, POP3 crée une copie locale de la boite et agit sur cette copie, ce qui permet de garder ses mails qu’ils soient ou non sur le
serveur.

IMAP (paramètres de serveur entrant)
------------------------------------

![](/static/img/man_mail/fr/k94.jpg)

Pour configurer K-9 comme un client IMAP, utilisez la procedure suivante:

 *Nom d’utilisateur:*
 Il s’agit simplement de votre adresse mail. K-9 enlève par défaut le nom de domaine, il faut donc le rajouter.
 
 *Mot de passe:*
 K-9 le garde en mémoire et le rempli automatiquement.
 
 *Serveur IMAP:*
 Le serveur IMAP de Autistici est mail.autistici.org
 
 *Type de sécurité:*
 Utilisez SSL. TLS ne semble pas marcher. Sélectionnez « SSL/TLS (toujours) ».
 
 *Type d’authentification:*
 Laissez le paramètre par défaut, il est correct. « PLAIN »
 
 *Port:*
 Utilisez le port 993, port par défaut du SSL pour IMAP.
 Laissez l’auto-détéction coché, et choisissez la compression selon vos désirs.

POP3 (paramètres de serveur entrant)
------------------------------------

![](/static/img/man_mail/fr/k95.jpg)

Pour configurer K-9 comme un client POP3, utilisez la procedure suivante:

 *Nom d’utilisateur:*
 Il s’agit simplement de votre adresse mail. K-9 enlève par défaut le nom de domaine, il faut donc le rajouter.
 
 *Mot de passe:*
 K-9 le garde en mémoire et le rempli automatiquement.
 
 *Serveur POP3:*
 Le serveur POP3 de Autistici est mail.autistici.org
 
 *Type de sécurité:*
 Utilisez SSL. TLS ne semble pas marcher. Sélectionnez « SSL/TLS (toujours) ».
 
 *Type d’authentification:*
 Laissez le paramètre par défaut, il est correct. « PLAIN »
 
 *Port:*
 Utilisez le port 995, port par défaut du SSL pour POP3.

SMTP (Paramètres de serveur sortant)
------------------------------------

![](/static/img/man_mail/fr/k96.jpg)

Pour la configuration du courier sortant (SMTP) procédez comme suit :

 *Serveur POP3 :*
 Le serveur SMTP de Autistici est smtp.autistici.org
 
 *Type de sécurité :* Utilisez SSL. TLS ne semble pas marcher. Sélectionnez « SSL/TLS (toujours) ».
 
 *Port :* Utilisez le port 465, port par défaut du SSL pour SMTP.
 
 *Laissez « Authentification requise » coché.*
 
 *Type d’authentification :*
 Laissez le paramètre par défaut, il est correct. «AUTOMATIC »
 
 *Nom d’utilisateur :*
 Il s’agit simplement de votre adresse mail. K-9 enlève par défaut le nom de domaine, il faut donc le rajouter.
 
 *Mot de passe :*
 K-9 le garde en mémoire et le rempli automatiquement.

