title: Documentazione dei servizi di posta
----

# Documentazione per gli utenti dei servizi di posta di A/I

* [Parametri diconnesione per i mail clients](/docs/mail/connectionparms)
* [Note tecniche sul servizio](/docs/mail/tech_mail)
* [Proteggere la propria privacy](/docs/mail/privacymail)
* [Guida della webmail](/docs/mail/roundcube)

## Istruzioni su come configurare i vari mail clients.

* [Evolution](/docs/mail/evolution)
* [Fetchmail](/docs/mail/fetchmail)
* [iPhone](/docs/mail/iphone)
* [K9 Mail](/docs/mail/k9mail)
* [Apple Mail (macOS)](/docs/mail/mail-osx)
* [Mutt](/docs/mail/mutt-msmtp)
* [Outlook](/docs/mail/outlook)
* [Sylpheed](/docs/mail/sylpheed)
* [Thunderbird](/docs/mail/thunderbird)

