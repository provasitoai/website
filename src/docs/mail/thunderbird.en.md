title: Thunderbird Configuration Howto
----

Thunderbird Configuration Howto
===================================

1) First of all you need to download and install Mozilla Thunderbird [here](http://www.mozilla.org/products/thunderbird/all.html)

Then follow the instructions below

2) Open Thunderbird and click *Edit* in the menu bar, then select *Account Settings*

![](/static/img/man_mail/en/tbA.jpg)

3) In the <em>Account Settings<em> window, click on *Add Account*

![](/static/img/man_mail/en/tbB.jpg)

4) When the *Account Wizard* window appears, click on the circle next to *Email account*, and then click *Next*.

![](/static/img/man_mail/en/tbC.jpg)

5) Fill in the desired values for *Your Name* (the name you want to be displayed on all the emails you send out) and *Email Address*, and
then click *Ok*.

![](/static/img/man_mail/en/tbD.jpg)

6) On the *Server Information* page, click the circle next to *POP*. In the field next to *Incoming Server* enter "mail.autistici.org". Then
click *Next*.

7) The *Incoming User Name* field should have your email address entered. If not insert it into the box and then click *Next*.

![](/static/img/man_mail/en/tbE.jpg)

8) The *Account Name* box should be filled with your email address. This value is used simply to identify the account, you may leave it as
the default or change it if you wish. When the settings are correct, click *Next* to continue.

![](/static/img/man_mail/en/tbF.jpg)

9) Review the information that is displayed. If errors appear, click *Back* to correct them as shown in the previous steps. When done click
*Finish*.

![](/static/img/man_mail/en/tbG.jpg)

10) At the *Account Settings* screen, in the lefthand window, select the account you've just created and click on the plus sign to expand
the menu. When the menu is expanded, click on *Server Settings*, check the box for *Use secure connection (SSL)* and verify that the *Port*
is listed as "995".

![](/static/img/man_mail/en/tbH.jpg)

11) The same operation should be repeated for the SMTP, the server managing the sending of emails. Click on *Outgoing Server* in the
lefthand window and write "smtp.autistici.org" in in the *Server Name* window and your email address in the *User Name* window.Then check
the SSL option and verify that the *Port* is listed as "465". If you selected the TLS option then the *Port* option can be listed as "25".

![](/static/img/man_mail/en/tbI.jpg)

Enjoy.
