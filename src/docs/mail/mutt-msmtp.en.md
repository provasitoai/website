title: Mutt+Msmtp Configuration Howto
----

Mutt+Msmtp Configuration Howto
======================================================

Goals:
------

-   to read mail with mutt from POP via TLS/SSL
-   to send authenticated mail via TLS/SSL withmutt+msmtp

Assumptions:
------------

-   you are already familiar with POP receiving in mutt
-   you have already configured mutt to send mail with mutt+msmtp
-   you have already an active account as USER@autistici.org and know your working PASSWORD


Reading email via POP using mutt
--------------------------------

For reading via POP in mutt, add and customize the following line (join the following!) in .muttrc, customizing PASSWORD and USER terms with
the ones provided in your [connection parameters](/docs/mail/connectionparms):

    	macro index <F8> ":set dsn_notify=<enter>:unhook *<enter>:unset
    	pop_user<enter>:unset pop_pass<enter>:set
    	pop_host=mail.autistici.org<enter>:set
    	pop_user=USER@DOMAIN.ORG<enter>:set
    	pop_pass=PASSWORD<enter><change-folder-readonly>
    	pops://USER@DOMAIN.ORG@mail.autistici.org<enter>:my_hdr From:
    	USER <USER@DOMAIN.ORG><enter>:set sendmail='/usr/bin/msmtp -a
    	USER'<enter>:reply_to From: USER
    	<USER@DOMAIN.ORG><enter>" "USER@DOMAIN.ORG POP"

Append the A/I certificate for POP mail to ~/.mutt/certificates and add the following to .muttrc :

    	set certificate_file=~/.mutt/certificates

(or mutt wil prompt you to add the certificate automatically)

Place the next in .muttrc ONLY if you have priority conflicts with other accounts :

    	set pop_authenticators="digest-md5:apop:user"

Sending email via STMP with msmtp
---------------------------------

For sending, place this in your default .msmtprc:

    	account USER@DOMAIN.ORG
    	port 587
    	from USER@DOMAIN.ORG
    	user USER@DOMAIN.org
    	auth on
    	password PASSWORD
    	tls on
    	tls_starttls on
    	tls_trust_file /etc/ssl/certs/ca-certificates.crt   # this file contains the Letsencrypt CA that signs our certificates.
                                                            # (it's contained in the ca-certificates package)
    	host smtp.autistici.org
    	tls_certcheck on

Test your msmtp configuration by mailing a "foo" message to yourself :

    	echo foo | msmtp -a USER USER@DOMAIN.ORG

