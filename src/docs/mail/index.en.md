title: Mail services documentation
----

# Documentation for users of the A/I mail service

* [Connection parameters for e-mail clients](/docs/mail/connectionparms)
* [Technical notes about the service](/docs/mail/tech_mail)
* [Protecting your privacy](/docs/mail/privacymail)
* [Webmail Howto](/docs/mail/roundcube)

## Instructions on how to set up various e-mail clients

* [Evolution](/docs/mail/evolution)
* [Fetchmail](/docs/mail/fetchmail)
* [iPhone](/docs/mail/iphone)
* [K9 Mail](/docs/mail/k9mail)
* [Apple Mail (macOS)](/docs/mail/mail-osx)
* [Mutt](/docs/mail/mutt-msmtp)
* [Outlook](/docs/mail/outlook)
* [Sylpheed](/docs/mail/sylpheed)
* [Thunderbird](/docs/mail/thunderbird)

