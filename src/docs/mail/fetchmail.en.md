title: Fetchmail Configuration Howto
----

Fetchmail Configuration Howto
==================================================

    Updated: 2016-04-06
    Keywords: fetchmail, SSL, Autistici, Inventati
    Description: how to configure fetchmail with SSL support for A/I mailboxes
    Language: en (English)
    Compatibility: fetchmail (SSL enabled) 

This mini Howto will teach you how to configure fetchmail to download A/I mail from our servers using SSL to ensure your privacy and how to
verify that no-one is sneaking on you while downloading your mail from our servers.

Fetchmail configuration file is *~/.fetchmailrc*: edit or create the file and add the following lines:

    # start A/I configuration
    poll "mail.autistici.org"
    proto imap
    user "user@domain.org"
    pass "password"
    ssl
    sslcertck
    sslcertpath "/etc/ssl/certs/"
    folder INBOX
    folder Spam
    nokeep
    # end A/I configuration


**WARNING:** to verify the certificates you have to install the ca-certificates package 


    Change the user@domain.org with your full e-mail address and
    password with your password.


    Note that you can omit the "password" line and fill it in every time you connect
    via fetchmail to the server. This protects your password from someone gaining
    undue access to your pc and reading your configuration file.


    The SSL option ensures the connection between you and the server happens through
    an encrypted channel while the ssl fingerprint is needed to confirm the
    fact that your are actually connecting to our servers and not to someone else
    just posing as our servers: it's a sort of verification of our servers identity.


    The folder option points out which mail folder you are interested in
    downloading (INBOX and Spam are usually more than enough), while nokeep
    means you do not want to keep a copy of your messages on the server. For more
    information you can read fetchmail manual (using the command line 'man 1
    fetchmail').


    Now you are ready, launch fetchmail and enjoy your messages
