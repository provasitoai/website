title: Il tuo threat model
----

Il tuo threat model
===================

Ovvero: Solo tu puoi sapere cosa vuoi proteggere e da chi
----------

Spesso nelle richieste di nuovi servizi troviamo frasi come "chiedo una
casella su A/I perché voglio proteggere la mia privacy", oppure "Mi serve per
essere anonimo". 

In realtà noi non possiamo garantire né l'una, né l'altra cosa, e se una
casella di posta (o altri servizi) sui nostri server può senz'altro essere un
passo verso la salvaguardia della tua privacy o del tuo anonimato, di certo non è
il primo.

Il primo passo da compiere, prima ancora di accendere il computer, è farti un
paio di domande:

- Cosa vuoi proteggere usando i nostri servizi?
- Da chi lo vuoi proteggere?
- Che cosa sai di queste persone/entità?
- Perché vorrebbero sorvegliarti o sapere chi sei?
- Che mezzi hanno usato finora per fare cose simili ad altri?
- Quante risorse hanno? Hanno alleati forti?
- Userebbero queste risorse o questi alleati per arrivare proprio a te?

Facciamo un paio di esempi:

### Vuoi un servizio su A/I per proteggere la tua privacy?

Noi non analizziamo la tua rete di contatti o i tuoi gusti e non cerchiamo di
venderti a qualche compagnia pubblicitaria, ma non possiamo neanche difenderti
da chi ha la violazione della tua privacy per modello di business. Se usi una
casella di posta di Autistici/Inventati per crearti un account su un servizio
commerciale che visiti regolarmente dal tuo solito browser, magari restando
tutto il tempo collegato a quel servizio e senza installarti plugin per
proteggere la tua privacy, avere una mailbox sui nostri server non servirà più
di tanto a raggiungere il tuo obiettivo.

Per proteggere la tua privacy, comincia a riflettere su quali servizi online
usi, e magari passa a servizi alternativi meno invasivi. Un'altra buona idea può
essere installare nel tuo browser un plugin che evita il tracciamento come
[Privacy Badger](https://www.eff.org/privacybadger) e un'estensione per bloccare
i banner pubblicitari come [uBlock Origin](https://github.com/gorhill/uBlock)
([Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)
- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)).


### Vuoi evitare che persone non autorizzate leggano le tue conversazioni?

Se scrivi dalla tua casella di posta a un'altra casella di posta su A/I,
tendenzialmente puoi stare abbastanza tranquilla che nessuno leggerà le tue mail
oltre alle persone a cui le mandi.

Ma dato che sicuramente alcune delle persone a cui scrivi usano servizi
commerciali che leggono la posta dei loro utenti per propinargli pubblicità
mirate, una casella di posta su A/I non può bastare da sola a proteggere le tue
conversazioni. Se poi stai parlando di argomenti particolarmente sensibili,
altri generi di persone (spesso in divisa) potrebbero essere interessate a
leggere la tua posta. Per essere certa che nessuno possa leggere la tua posta oltre a te e alle persone a cui scrivi, ti consigliamo quindi di usare GPG per crittare almeno i messaggi piu' delicati.

Puoi trovare spiegazioni in italiano sulla crittazione della posta [in questa
pagina sulle mail sicure](mail/privacymail), ma se capisci l'inglese ti
consigliamo di leggere [questa guida più
aggiornata](https://securityinabox.org/en/guide/secure-communication/) su
Security in a Box.

### Quel che ti serve è l'anonimato?

È vero, noi non teniamo traccia del tuo indirizzo IP e non abbiamo modo di
sapere chi sei o da dove ti colleghi. Ma se ci tieni a salvaguardare il tuo
anonimato, una casella di posta su A/I è solo uno dei tanti passi necessari.

Prima di tutto sarebbe meglio fare chiarezza su che cosa intendi per anonimato e
a chi vuoi nascondere la tua identità ufficiale. Se vuoi che gli amici con cui
ti scambi le mail non sappiano il tuo nome di battesimo, quello che vuoi è usare
uno pseudonimo. Per questo, una casella su A/I può bastare.

Ma se vuoi evitare che la polizia o altri poteri risalgano alla tua vera
identità, sarà meglio che installi [Tor Browser](https://www.torproject.org/) e
che leggi bene le [raccomandazioni per usarlo
correttamente](https://www.torproject.org/download/download.html.en#warning)
(purtroppo solo in inglese).

Se il rischio che la tua identità venga rivelata porterebbe a conseguenze
intollerabili per te, ti raccomandiamo di proteggerti ancora meglio usando
[Tails](https://tails.boum.org/index.it.html) per tutte le tue attività più
sensibili.

Se invece vuoi spedire messaggi in modo totalmente anonimo puoi usare gli
anonymous remailer. Puoi saperne di più iniziando da [questa
pagina](https://www.autistici.org/services/anon).

### Threat modeling per le situazioni più delicate

Prima di usare la tua casella di posta o altri servizi sui nostri server per
attività delicate, insomma, **ti raccomandiamo caldamente** di analizzare i
rischi che corri e come limitarli con un esercizio di *threat modeling*.

Per sapere di più su come fare, puoi leggere la sezione "Threat Modeling" della
guida [Le basi della sicurezza
digitale](https://motherboard.vice.com/it/article/d3devm/la-guida-di-motherboard-per-non-farsi-hackerare)
di Motherboard e [questa guida dell'EFF in
inglese](https://ssd.eff.org/en/module/assessing-your-risks).
