title: Héberger un site avec son propre Nom de Domaine (DNS) 
----

Héberger un site avec son propre Nom de Domaine (DNS) 
==========================================================================

A/I peut également héberger votre site avec son propre Nom de domaine (DNS). En revanche, nous ne l'enregistrons pas nous-même. Pour ce
faire, adressez-vous à un Registrar commercial, tel que :

- [GANDI](http://www.gandi.net/)

Avant d'acheter un nom de domaine, considérez que l'enregistrement de certaines DNS de premier niveau (`.it` par exemple) nécessitent une
pièce d'identité. Votre nom sera alors associé au domaine.

Pour ensuite configurer votre domaine en utilisant les serveurs de noms A/I, veuillez utiliser les serveurs de noms suivants
(l'enregistrement des Serveurs de Noms \[NS\] nécessite la saisie des noms et non des IPs) :

<pre>{{range .dns.servers}}
{{.name}}    {{.ip}}{{end}}</pre>

N'oubliez pas d'accéder à votre Panneau de gestion de domaine ! Il est possible qu'en de rares occasions, les IPs changent : il vous faudra
alors mettre à jour la configuration de votre domaine. Nous donnerons des indications sur ces changements éventuels sur [notre blog](https://cavallette.noblogs.org).
