title: Algunas notas sobre la privacidad
----

Algunas notas sobre la privacidad de los usuarios
=================================================

A/I se esfuerza por no guardar información en sus servidores que pudiera ayudar a identificar a la gente que navega en sus sitios. Por esta
razón es muy importante que los webmasters presten atención a las funciones del software que eligen instalar: muchos contadores de visitas y
servicios de CMS registran las IP de los visitantes (independientemente de la configuración global de los servidores de A/I), y es crucial
verificar que estas funciones estén desactivadas en vuestros sitios para asegurar que nuestras intenciones, respecto a la privacidad, se
cumplan.

Nuestros servidores ofrecen muchos servicios diferentes y son, a día de hoy, una importante herramienta
de comunicación para miles de activistas. Esperamos que estar al tanto de este hecho, ayude a reducir los problemas que puedan surgir debido
al uso indiscriminado de tu espacio web.

Por último, pero no menos importante, nuestros servidores sólo mantienen registros para operaciones de depuración estrictamente necesarias.
