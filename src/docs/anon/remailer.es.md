title: Anonymous Remailer Howto
----

Anonymous Remailer - Howto
==========================

Estas son las instrucciones básicas para entender cómo se crean los mensajes con remailer o repetidor de correo: si utilizas un cliente
específico no tendrás que aprender este proceso, pero puede ayudarte de todos modos a entender cómo funcionan los repetidores de correo.

Si deseas enviar un mensaje anónimo, crea primero un archivo que contenga:

- Dos signos de dos puntos (::) en la primera línea,
- La frase "Anon-To: dirección de correo electrónico" en la segunda línea,

donde la dirección de correo electrónico debe ser la dirección a la que el repetidor de correo enviará el mensaje.

La tercera línea debe estar vacía e irá seguida del texto del mensaje.

 Ej.:

	============================================================= 
	:: Anon-To: destinatario@ejemplo.org  

	Escriba aquí su mensaje 
	=============================================================

Los repetidores de correo sólo aceptan mensajes cifrados mediante PGP o GPG, por lo que tu mensaje debe ser cifrado con la clave pública del
repetidor de correo, que se puede obtener mediante el envío de un mensaje al repetidor de correo (mixmaster@remailer.paranoici.org) y
mediante la introducción de "remailer-key" en el asunto.

Así, el mensaje anterior debe ser cifrado con la clave PGP del repetidor de correo y finalmente enviado a mixmaster@remailer.paranoici.org
mediante la introducción de dos signos de dos puntos al comienzo del mensaje y, en la segunda línea, la frase "Encrypted: PGP", seguida del
mensaje previamente cifrado.

	============================================================= 
	De: joe@test.com 
	Para: mixmaster@remailer.paranoici.org  

	:: 
	Encrypted: PGP  

	-----BEGIN PGP MESSAGE----- 
	Version: 2.6.3i  
	owE1jMsNwjAUBH3gZMk9PClnUoBPUANpwElW2OBPZD8H0gd1UCP2gduuNDNfj 
	IcSHT4zCbQmtlbzGFM9T0jSD7QVvEzaPcUlBSSWHQclbnR9YWJNp5BFSLdR9s 
	CijF3NGxybry/1Rsqn4la3a0JiIhLvnYGCu9HFtiC8oIxnlkeuIYe+EH=HgDa 
	-----END PGP MESSAGE----- 
	=============================================================

El repetidor de correo decodificará el mensaje y lo enviará de forma anónima. Si quieres incluir un asunto u otras cabeceras que no deban
ser filtradas por el repetidor de correo, puedes introducirlos como se explica a continuación antes de cifrar el mensaje para el repetidor
de correo:

	============================================================= 
	:: 
	Anon-To: destinatario@ejemplo.org  
	## 
	Asunto: Re: Twofish 
	En respuesta a: Your message of "Tue, 12 Jan 1999 22:47:04 EST." <199901130247.WAA02761@example.com>  

	Texto del mensaje 
	=============================================================

Aunque el cifrado PGP es muy seguro, utilizar un repetidor de correo de la forma más básica no es el mejor método para proteger tu
identidad. Podrías decirle al repetidor de correo, por ejemplo, que retenga el mensaje que has enviado durante un determinado período de
tiempo y que lo remita posteriormente a fin de evitar el llamado análisis de tráfico.

Si introduces en la cabecera `Latent-Time: +2:00`, tu mensaje será retrasado por 2 horas, mientras que si introduces la sintaxis
`Latent-Time: +5:00r`, obtendrás un retraso aleatorio entre 0 y 5 horas.

La mejor manera de utilizar los repetidores de correo es utilizándolos en cadena, enviando un mensaje de un repetidor de correo a otro
antes de que llegue a su destinatario.

Veamos un ejemplo con el mensaje anterior:

	============================================================= 
	:: 
	Anon-To: mixmaster@remailer.paranoici.org  

	:: Encrypted: PGP  

	-----BEGIN PGP MESSAGE----- 
	Version: 2.6.3i  
	owE1jMsNwjAUBH3gZMk9PClnUoBPUANpwElW2OBPZD8H0gd1UCP2gduuNDNfI 
	T4zCbQmtlbzGFM9T0jSD7QVvEzaPcUlBSSWHQclbnR9YWJNp5BFSLdR9CijF3 
	ybry/1Rsqn4la3a0JiIhLvnYGCu9HFtiC8oIxnlkeuIYe+EH=HgDq 
	-----END PGP MESSAGE----- 
	=============================================================`

Puedes cifrar el mensaje con la clave PGP de otro repetidor de correo (ej. Cripto) y enviar el mensaje cifrado a este repetidor de correo:
<anon@ecn.org>

De esta manera Cripto recibirá y decodificará el mensaje y, además, encontrará las instrucciones para enviarlo a
mixmaster@remailer.paranoici.org, quien a su vez lo decodificará y enviará a destinatario@ejemplo.org

Puedes encontrar una lista de software criptográfico y clientes repetidores de correo aquí: [autistici.org/crypto](https://autistici.org/crypto) y algo de información adicional
en nuestra página web sobre repetidores de correo: <http://remailer.paranoici.org>
