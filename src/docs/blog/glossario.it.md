title: Glossario per blogger
----

Glossario per blogger
=====================


**UN BLOG (O WEBLOG) È UN SITO WEB PERSONALE**

-   contenente principalmente notizie (post)
-   aggiornato regolarmente
-   sotto forma di diario (i post più recenti si trovano in cima alla pagina) con molti post organizzati in categorie
-   che per essere installato richiede uno strumento interattivo specifico
-   di norma creato e gestito da una sola persona, a volte anonimo


**I POST DEI BLOG:**

-   sono spesso di solo testo (con link esterni), ma a volte contengono immagini e spesso anche suoni e video.
-   possono essere commentati dai lettori
-   vengono archiviati nel blog e possono essere consultati costantemente


**QUINDI UN BLOG È SIMILE A UNA PAGINA WEB PERSONALE, MA**

-   È più facile da mettere su e da gestire ed è molto più attivo e frequentemente aggiornato
-   favorisce uno stile più aperto e personale e opinioni più dirette
-   promuove ampiamente dibattiti con i visitatori e altri blogger
-   stabilisce un formato di blog diffuso in tutto il mondo, con metodi simili (due o tre colonne di layout, commenti ai post e feed RSS.


**BLOG**

Diminutivo di Weblog. Un sito che contiene materiale scritto, link o fotografie che vengono aggiunte continuamente, di solito da una sola
persona a sua discrezione.

**BLOGGARE**

Gestire un blog o postare materiale in un blog.

**BLOGGER**

La persona che gestisce un blog.

**BLOGOSFERA**

Tutti i blog o la comunità di blog.

**BLOGROLL**

Lista di link esterni che appaiono in un blog; spesso si tratta di link ad altri blog e sono di norma disposti in colonna nella pagina
principale. Spesso il blogroll corrisponde a una sottocomunità di blogger amici.

**BLOGWARE**

Software usato per gestire un blog.

**COMMENT SPAM**

Come lo spam con le e-mail. Le macchine Spambot sommergono un blog con pubblicità travestita da commenti. Si tratta di un problema molto
serio che costringe i blogger e le piattaforme blog ad attrezzarsi con strumenti adatti a escludere alcuni utenti e a impedire a determinati
indirizzi di pubblicare commenti.

**CONTENT SYNDICATION (condivisione contenuti)**

Si parla di "content syndication" quando l'autore o l'amministratore autorizza completamente, o in parte, l'uso dei contenuti affinché
vengano utilizzati da un altro sito.

**MOBLOG**

Contrazione di mobile blog, blog da cellulare. Un blog che può essere aggiornato a distanza da qualsiasi luogo, sia tramite telefono che
con un altro supporto digitale.

**PERMALINK**

Contrazione di permanent link, collegamento permanente. L'indirizzo web di ogni elemento postato nel blog. Un modo pratico di tenere tra i
preferiti un post, anche quando sia già stato archiviato dal blog in cui era contenuto.

**PHOTOBLOG**

Un blog composto per la maggior parte da fotografie, inviate continuamente e in ordine cronologico.

**PODCASTING**

Contrazione di iPod e broadcasting, emittente. Postare materiale audio e video su di un blog e il suo RSS, per apparecchi digitali.

**POST**

Un elemento inviato a un blog. Si può trattare di messaggi o notizie, o anche di foto o solo lin. Spesso breve, con link esterni e con la
possibilità di lasciare commenti.

**RSS (REALLY SIMPLE SYNDICATION)**

Un modo di trattare gli elementi più recenti di un sito, adatto in particolare manier ai blog, poiché avverte gli utentiogni volta che i
loro blog preferiti vengono aggiornati. Può anche autorizzare altri siti web a riprodurre (in maniera semlice e automatica) per intero o in
parte i contenuti di un sito. Si sta diffondendo velocemente, in particolare nei siti di informazione.

**RSS AGGREGATOR**

Software o servizi online che permettono ai blogger di leggere un RSS feed, in particolare i post più recenti del loro blog favorito. Viene
chiamato anche reader, o feedreader.

**RSS FEED**

Il file che contiene gli ultimi post del blog. Viene letto da un RSS aggregator/reader (aggregatore/lettore di RSS) e comunica in tempo
reale un aggiornamento al blog.

**TRACKBACK**

Un modo utilizzato dai siti per comunicare automaticamente quando un elemento postato si riferisce a un commento precedente.

**WEB DIARY**

Un blog.

**WIKI**

Dalla parola hawaiana wikiwiki ("veloce"). Un sito web che può essere aggiornato facilmente e velocemente da qualsiasi utente. La parola
adesso si riferisce anche agli strumenti utilizzati per creare un wiki (wiki engines, motori wiki). Nonostante i blog e i wiki abbiano
alcune caratteristiche in comune, rimangono abbastanza differenti.
