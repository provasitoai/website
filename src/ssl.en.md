title: SSL: an encrypted tunnel to secure your communications
----

SSL: an encrypted tunnel to secure your communications
======================================================

When you connect to a website through **https** or when you configure your mail,
Jabber or IRC client to connect with SSL encryption, your computer receives
from A/I's servers an SSL certificate, which makes it possible to create an
encrypted tunnel to protect your communications.

Until recently, A/I chose not to rely on a commercial certification authority
(CA), because that would have put the trust relationship between us and our
users into the hands of subjects that can’t be trusted: nation states, law
enforcement agencies, corporations who only care about profit.

Then in December 2015 [Let's Encrypt](https://letsencrypt.org/) was launched:
co-founded by the [Electronic Frontier Foundation](https://www.eff.org/)),
Let's Encrypt issues free and easy-to-install SSL certificates with the
explicit aim of facilitating and spreading the use of web cryptography.
So now all of A/I's services are encrypted with SSL keys signed by Let's
Encrypt. These keys are automatically recognized by your browsers and clients:
if you open this page in HTTPS instead of HTTP, you will see a green padlock in
the address bar, which means that your connection is secure.

This means that you don't need to do anything to establish a secure connection
with our servers, and we just recommend you to check that your clients are not
set to accept invalid certificates (as you once needed to do for Xchat, for
instance).

The only service we don't use Let's Encrypt for is
[Noblogs](https://noblogs.org/), for which we buy a commercial certificate. The
structure of the platform would force us to create a certificate for each blog,
which would be a huge waste of resources for Let's Encrypt. The solution is a
wildcard certificate (which can be used to secure several domains at once),
which unfortunately Let's Encrypt doesn't allow for (yet). 
